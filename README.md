# mmap-storage

[![Build](https://gitlab.com/Douman/mmap-storage/badges/master/build.svg)](https://gitlab.com/Douman/mmap-storage/pipelines)
[![Crates.io](https://img.shields.io/crates/v/mmap-storage.svg)](https://crates.io/crates/mmap-storage)
[![Documentation](https://docs.rs/mmap-storage/badge.svg)](https://docs.rs/crate/mmap-storage/)

Memory map backed storage.

It provides File and Anonymous based storages.

## Features

- `serializer` - Enables `serializer` module that provides file view onto `serde` compatible data
- `toml` - Enables `toml` serialization.
- `json` - Enables `json` serialization.
- `bincode` - Enables `bincode` serialization.
