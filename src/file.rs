//!File backed memory mapped storage
use std::fs;
use std::path;
use std::io;

/// File memory map backed storage.
///
/// Attempting to resize storage results in error.
pub struct Storage {
    //In order to use regular file APIs
    //we need to drop MmapMut
    //As Rust would not let us move out
    //of &mut we need to wrap it into Option :(
    inner: Option<memmap::MmapMut>,
    file: fs::File,
    //Cursor with end
    end: usize,
    //Total size of storage
    size: usize,
}

impl Storage {
    fn inner_open(file: fs::File) -> io::Result<Self> {
        let metadata = file.metadata()?;
        let mut size = metadata.len() as usize;
        let end = size;
        if size == 0 {
            file.set_len(1)?;
            size = 1;
        }
        let mut result = Self {
            inner: None,
            file,
            end,
            size
        };

        result.mmap().map(move |_| result)
    }

    /// Creates storage by opening or creating file at the specified location.
    ///
    /// If file is newly created it is appended dummy byte.
    pub fn open<P: AsRef<path::Path>>(path: P) -> io::Result<Self> {
        let file = fs::OpenOptions::new().read(true).write(true).create(true).open(&path)?;
        Self::inner_open(file)
    }

    /// Opens storage, if it doesn't exist return error.
    pub fn open_exising<P: AsRef<path::Path>>(path: P) -> io::Result<Self> {
        let file = fs::OpenOptions::new().read(true).write(true).open(&path)?;
        Self::inner_open(file)
    }

    fn as_inner(&self) -> &memmap::MmapMut {
        match self.inner.as_ref() {
            Some(inner) => inner,
            None => unreachable!()
        }
    }

    fn as_inner_mut(&mut self) -> &mut memmap::MmapMut {
        match self.inner.as_mut() {
            Some(inner) => inner,
            None => unreachable!()
        }
    }

    fn manual_drop(&mut self) {
        self.inner.take();
    }

    fn mmap(&mut self) -> io::Result<()> {
        self.inner = unsafe {
            Some(memmap::MmapMut::map_mut(&self.file)?)
        };

        Ok(())
    }

    fn set_len(&mut self, new_size: usize) -> io::Result<()> {
        let file_size = match new_size {
            0 => 1,
            n => n as u64
        };
        self.file.set_len(file_size)?;
        self.size = file_size as usize;
        if self.end > new_size {
            self.end  = new_size;
        }
        self.end = new_size;
        Ok(())
    }

    #[inline]
    /// Returns slice with view on written data.
    pub fn as_slice(&self) -> &[u8] {
        &self.as_inner()[..self.end]
    }

    #[inline]
    /// Returns slice with view on written data.
    pub fn as_mut_slice(&mut self) -> &mut [u8] {
        let end = self.end;
        &mut self.as_inner_mut()[..end]
    }

    #[inline]
    /// Returns overall size of storage(including extra space).
    pub fn capacity(&self) -> usize {
        self.size
    }

    #[inline]
    /// Asynchronously flushes outstanding memory map modifications to disk.
    ///
    /// This method initiates flushing modified pages to durable storage, but it will not wait for
    /// the operation to complete before returning.
    pub fn flush_async(&self) -> io::Result<()> {
        self.as_inner().flush_async()
    }

    #[inline]
    /// Synchronously flushes outstanding memory map modifications to disk.
    pub fn flush_sync(&self) -> io::Result<()> {
        self.as_inner().flush()
    }

    /// Resizes storage by appending or truncating.
    ///
    /// It modifies file size and re-mmaps file.
    /// When Attempting to set size to 0, the actual size will
    /// become 1.
    ///
    /// If current cursor of data becomes out of actual
    /// file storage, it is shifted to actual file storage size
    pub fn resize(&mut self, new_size: usize) -> io::Result<()> {
        self.manual_drop();
        match self.set_len(new_size) {
            Ok(_) => (),
            Err(error) => {
                //In case of error we must restore original
                //mmap otherwise we gonna use uninitialized
                //value
                self.mmap()?;
                return Err(error);
            }
        }
        self.mmap()
    }

    ///Resizes map accordingly to data and copies it.
    ///
    ///Convenience method to map buffer to file.
    pub fn put_data(&mut self, data: &[u8]) -> io::Result<()> {
        assert!(data.len() > 0);
        if self.size != data.len() {
            self.resize(data.len())?;
        }
        let size = self.size;
        self.as_inner_mut()[..size].copy_from_slice(data);
        self.end = self.size;
        Ok(())
    }

    /// Appends data to storage
    ///
    /// Note that it resizes storage if needed.
    /// Therefore error can happen.
    pub fn extend_from_slice(&mut self, data: &[u8]) -> io::Result<()> {
        let old_end = self.end;
        if (self.size - self.end) < data.len() {
            let new_size = self.end + data.len();
            self.resize(new_size)?;
        }
        let new_end = old_end + data.len();
        self.as_inner_mut()[old_end..new_end].copy_from_slice(data);
        self.end = new_end;
        Ok(())
    }

    /// Copies data from slice into storage.
    ///
    /// Function panics if data has greater len.
    ///
    /// **Note:** It copies data up to storage capacity.
    pub fn copy_from_slice(&mut self, data: &[u8]) {
        assert!(self.size >= data.len());
        self.end = data.len();
        self.as_mut_slice().copy_from_slice(data);
    }

    /// Creates `Vec` from content of storage.
    pub fn to_vec(&self) -> Vec<u8> {
        let mut result = Vec::with_capacity(self.end);
        result.extend_from_slice(self.as_slice());
        result
    }
}

impl io::Write for Storage {
    #[inline]
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        match buf.len() {
            0 => Ok(0),
            n => self.extend_from_slice(buf).map(|_| n)
        }
    }

    #[inline]
    fn write_all(&mut self, buf: &[u8]) -> io::Result<()> {
        match buf.len() {
            0 => Ok(()),
            _ => self.extend_from_slice(buf)
        }
    }

    #[inline]
    fn flush(&mut self) -> io::Result<()> {
        self.as_inner().flush()
    }
}
