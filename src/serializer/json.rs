//! Json serialization module
use serde::{Serialize, Deserialize};

use std::io;

use crate::file as inner;

use super::Serialization;

#[derive(Default)]
/// Json Serializer.
///
/// Uses pretty format for serialization.
///
/// Note that empty data writes `{}` into file
pub struct Json;

impl<'de, T: Serialize + Deserialize<'de>> Serialization<'de, T> for Json {
    fn serialize(data: &T) -> Result<Vec<u8>, io::Error> {
        serde_json::to_vec_pretty(data).map_err(|error| io::Error::new(io::ErrorKind::Other, error))
    }
    fn serialize_into(data: &T, writer: &mut inner::Storage) -> Result<(), io::Error> {
        writer.resize(0)?;
        let buffer = io::BufWriter::new(writer);
        serde_json::to_writer_pretty(buffer, data).map_err(|error| io::Error::new(io::ErrorKind::Other, error))
    }
    fn deserialize(bytes: &'de [u8]) -> Result<T, io::Error> {
        serde_json::from_slice(bytes).map_err(|error| io::Error::new(io::ErrorKind::Other, error))
    }
}

#[cfg(test)]
mod tests {
    use super::Json;
    use super::super::{FileView};

    use std::fs;
    use std::collections::HashMap;

    #[test]
    fn should_handle_json_file_view() {
        const STORAGE_PATH: &'static str = "test_file_view.json";
        let _ = fs::remove_file(STORAGE_PATH);

        {
            //Try to just open non-existing data
            let storage = FileView::<HashMap<String, String>, Json>::open(STORAGE_PATH).expect("To create file");
            let result = storage.load();
            //We cannot load from dummy bytes
            assert!(result.is_err());
        }

        let _ = fs::remove_file(STORAGE_PATH);

        {
            //Try to open non-existing file so that it could use provided default
            let mut default_map = HashMap::new();
            default_map.insert(1.to_string(), "".to_string());
            let storage = FileView::<HashMap<String, String>, Json>::open_or(STORAGE_PATH, &default_map).expect("Open with provided default data");;
            let data = storage.load().expect("To load data from empty file");
            assert_eq!(data.len(), default_map.len());
            assert_eq!(data.get(&1.to_string()).expect("To find key 1"), "");
        }

        let _ = fs::remove_file(STORAGE_PATH);

        let new_len = {
            //Try to open non-existing file so that it would write {}
            let mut storage = FileView::<HashMap<String, String>, Json>::open_or_default(STORAGE_PATH).expect("To open empty file");
            let mut data = storage.load().expect("To load data from empty file");
            assert_eq!(data.len(), 0);

            data.insert(1.to_string(), "".to_string());
            data.insert(2.to_string(), "two".to_string());

            data.insert(3.to_string(), "".to_string());
            data.insert(4.to_string(), "".to_string());

            data.insert(5.to_string(), "".to_string());
            data.insert(4.to_string(), "four".to_string());

            storage.save_sync(&data).expect("To save modified data");

            data.len()
        };

        let expected = [
            (1, ""),
            (2, "two"),
            (3, ""),
            (4, "four"),
            (5, "")
        ];

        {
            //Json should be able to load with usize instead of String
            let mut default_data = HashMap::new();
            default_data.insert(1, "one".to_string());
            let storage = FileView::<HashMap<usize, String>, Json>::open_or(STORAGE_PATH, &default_data).expect("To load json");
            let data = storage.load().expect("To load json data");

            assert_eq!(data.len(), new_len);
            for (expected_key, expected_val) in expected.iter() {
                let expected_val: &str = expected_val;
                let value: &str = data.get(&expected_key).expect("To find key");
                assert_eq!(value, expected_val);
            }
        }

        {
            // Load valid data
            let mut storage = FileView::<HashMap<String, String>, Json>::open(STORAGE_PATH).expect("To open file");
            let data = storage.load().expect("To load data");

            // We should read saved data
            assert_eq!(data.len(), new_len);
            for (expected_key, expected_val) in expected.iter() {
                let expected_val: &str = expected_val;
                let value: &str = data.get(&expected_key.to_string()).expect("To find key");
                assert_eq!(value, expected_val);
            }

            storage.save_sync(&data).expect("To save data");
            //Check owned load
            let _data = storage.load_owned().expect("To load owned data");
        }

        {
            // Load valid data
            let mut storage = FileView::<HashMap<&str, &str>, Json>::open(STORAGE_PATH).expect("To open file");

            storage.modify(|data| {
                data
            }).expect("To modify");

            let data = storage.load().expect("To load data");
            // We should read saved data
            assert_eq!(data.len(), new_len);
            for (expected_key, expected_val) in expected.iter() {
                let expected_val: &str = expected_val;
                let value: &str = data.get(&expected_key.to_string().as_ref()).expect("To find key");
                assert_eq!(value, expected_val);
            }
        }

        let _ = fs::remove_file(STORAGE_PATH);
    }

}
