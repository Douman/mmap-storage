//! Bincode serialization module

use serde::{Serialize, Deserialize};

use std::io;

use crate::file as inner;

use super::Serialization;

#[derive(Default)]
/// Bincode Serializer.
pub struct Bincode;

impl<'de, T: Serialize + Deserialize<'de>> Serialization<'de, T> for Bincode {
    fn serialize(data: &T) -> Result<Vec<u8>, io::Error> {
        bincode::serialize(data).map_err(|error| io::Error::new(io::ErrorKind::Other, error))
    }

    fn serialize_into(data: &T, writer: &mut inner::Storage) -> Result<(), io::Error> {
        writer.resize(0)?;
        let buffer = io::BufWriter::new(writer);
        bincode::serialize_into(buffer, data).map_err(|error| io::Error::new(io::ErrorKind::Other, error))
    }
    fn deserialize(bytes: &'de [u8]) -> Result<T, io::Error> {
        bincode::deserialize(bytes).map_err(|error| io::Error::new(io::ErrorKind::Other, error))
    }
}

#[cfg(test)]
mod tests {
    use super::Bincode;
    use super::super::{FileView};

    use std::fs;
    use std::collections::HashMap;

    #[test]
    fn should_handle_bincode_file_view() {
        const STORAGE_PATH: &'static str = "test_file_view.bincode";
        let _ = fs::remove_file(STORAGE_PATH);

        {
            //Try to just open non-existing data
            let storage = FileView::<HashMap<String, String>, Bincode>::open(STORAGE_PATH).expect("To create file");
            let result = storage.load();
            //We cannot load from dummy bytes
            assert!(result.is_err());
        }

        let _ = fs::remove_file(STORAGE_PATH);

        {
            //Try to with default
            let storage = FileView::<HashMap<String, String>, Bincode>::open_or_default(STORAGE_PATH).expect("To open with default");
            let result = storage.load().expect("To load with default");
            assert_eq!(result.len(), 0);
        }

        let _ = fs::remove_file(STORAGE_PATH);

        {
            //Try to open non-existing file so that it could use provided default
            let mut default_map = HashMap::new();
            default_map.insert(1.to_string(), "".to_string());
            let storage = FileView::<HashMap<String, String>, Bincode>::open_or(STORAGE_PATH, &default_map).expect("Open with provided default data");;
            let data = storage.load().expect("To load data from empty file");
            assert_eq!(data.len(), default_map.len());
            assert_eq!(data.get(&1.to_string()).expect("To find key 1"), "");
        }

        let _ = fs::remove_file(STORAGE_PATH);

        let new_len = {
            //Just open
            let mut storage = FileView::<HashMap<String, String>, Bincode>::open_or_default(STORAGE_PATH).expect("To create file");
            let mut data = storage.load().expect("To load data from empty file");
            assert_eq!(data.len(), 0);

            data.insert(1.to_string(), "".to_string());
            data.insert(2.to_string(), "two".to_string());

            data.insert(3.to_string(), "".to_string());
            data.insert(4.to_string(), "".to_string());

            data.insert(5.to_string(), "".to_string());
            data.insert(4.to_string(), "four".to_string());

            storage.save_sync(&data).expect("To save modified data");

            data.len()
        };

        let expected = [
            (1, ""),
            (2, "two"),
            (3, ""),
            (4, "four"),
            (5, "")
        ];

        {
            // Load valid data
            let mut storage = FileView::<HashMap<String, String>, Bincode>::open(STORAGE_PATH).expect("To open file");
            let data = storage.load().expect("To load data");

            // We should read saved data
            assert_eq!(data.len(), new_len);
            for (expected_key, expected_val) in expected.iter() {
                let expected_val: &str = expected_val;
                let value: &str = data.get(&expected_key.to_string()).expect("To find key");
                assert_eq!(value, expected_val);
            }

            storage.save_sync(&data).expect("To save data");
            //Check owned load
            let _data = storage.load_owned().expect("To load owned data");
        }

        {
            // Load valid data
            let mut storage = FileView::<HashMap<&str, &str>, Bincode>::open(STORAGE_PATH).expect("To open file");

            storage.modify(|data| {
                data
            }).expect("To modify");

            let data = storage.load().expect("To load data");

            // We should read saved data
            assert_eq!(data.len(), new_len);
            for (expected_key, expected_val) in expected.iter() {
                let expected_val: &str = expected_val;
                let value: &str = data.get(&expected_key.to_string().as_ref()).expect("To find key");
                assert_eq!(value, expected_val);
            }
        }

        let _ = fs::remove_file(STORAGE_PATH);
    }
}
