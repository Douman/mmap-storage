//! Provides memory map backed storage
//!
//! Memory map backed storage.
//!
//! It provides File and Anonymous based storages.
//!
//! ## Features
//!
//! - `serializer` - Enables `serializer` module that provides file view onto `serde` compatible
//! data
//! - `toml` - Enables `toml` serialization.
//! - `json` - Enables `json` serialization.
//! - `bincode` - Enables `bincode` serialization.
#![deny(missing_docs)]
#![cfg_attr(feature = "cargo-clippy", allow(clippy::style))]

pub mod anonim;
pub mod file;
#[cfg(feature = "serializer")]
pub mod serializer;
