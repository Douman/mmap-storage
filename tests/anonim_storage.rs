extern crate mmap_storage;

use mmap_storage::anonim::Storage;

#[test]
fn test_it_works() {
    let data = [4, 5, 1, 6, 8, 1];
    let mut storage = Storage::new(1024).expect("To crate mmap storage");

    storage.extend_from_slice(&data).expect("To extend with data");
    storage.flush_sync().expect("To flush");
    assert_eq!(storage.as_slice().len(), data.len());
    assert_eq!(storage.capacity(), 1024);
    assert_eq!(storage.as_slice(), data);

    let vec = storage.to_vec();
    assert_eq!(vec.capacity(), data.len());
    assert_eq!(vec, data);
}

#[test]
fn test_mmap_storage_double_realloc() {
    let data = [4, 5, 1, 6, 8, 1];
    let mut storage = Storage::new(4).expect("To crate mmap storage");

    storage.extend_from_slice(&data).expect("To extend with data");
    storage.flush_sync().expect("To flush");
    assert_eq!(storage.as_slice().len(), data.len());
    assert_eq!(storage.capacity(), 8);
    assert_eq!(storage.as_slice(), data);

    let vec = storage.to_vec();
    assert_eq!(vec.capacity(), data.len());
    assert_eq!(vec, data);
}

#[test]
fn test_mmap_storage_increase_by_new_data_size() {
    let data = [4, 5, 1, 6, 8, 1];
    let mut storage = Storage::new(1).expect("To crate mmap storage");

    storage.extend_from_slice(&data).expect("To extend with data");
    storage.flush_sync().expect("To flush");
    assert_eq!(storage.as_slice().len(), data.len());
    assert_eq!(storage.capacity(), data.len());
    assert_eq!(storage.as_slice(), data);

    let vec = storage.to_vec();
    assert_eq!(vec.capacity(), data.len());
    assert_eq!(vec, data);
}

#[test]
fn test_multiple_appends() {
    let data = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    //Test with realloc
    let mut storage = Storage::new(1).expect("To crate mmap storage");

    storage.extend_from_slice(&data[..4]).expect("To extend with data");
    storage.flush_sync().expect("To flush");
    assert_eq!(storage.as_slice().len(), 4);
    assert_eq!(storage.capacity(), 4);
    assert_eq!(storage.as_slice(), &data[..4]);

    storage.extend_from_slice(&data[4..]).expect("To extend with data");
    storage.flush_sync().expect("To flush");
    assert_eq!(storage.as_slice().len(), data.len());
    assert_eq!(storage.capacity(), data.len());
    assert_eq!(storage.as_slice(), data);

    let vec = storage.to_vec();
    assert_eq!(vec.capacity(), data.len());
    assert_eq!(vec, data);

    //Test without realloc
    let mut storage = Storage::new(1024).expect("To crate mmap storage");

    storage.extend_from_slice(&data[..4]).expect("To extend with data");
    storage.flush_sync().expect("To flush");
    assert_eq!(storage.as_slice().len(), 4);
    assert_eq!(storage.capacity(), 1024);
    assert_eq!(storage.as_slice(), &data[..4]);

    storage.extend_from_slice(&data[4..]).expect("To extend with data");
    storage.flush_sync().expect("To flush");
    assert_eq!(storage.as_slice().len(), data.len());
    assert_eq!(storage.capacity(), 1024);
    assert_eq!(storage.as_slice(), data);

    let vec = storage.to_vec();
    assert_eq!(vec.capacity(), data.len());
    assert_eq!(vec, data);
}

#[test]
fn test_copy_from_slice() {
    let data = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    let seco = [9, 7, 5, 4, 2, 0, 8, 2, 1];
    //Test with realloc
    let mut storage = Storage::new(1).expect("To crate mmap storage");

    storage.extend_from_slice(&data).expect("To extend storage");
    storage.flush_sync().expect("To flush");
    assert_eq!(storage.as_slice().len(), data.len());
    assert_eq!(storage.capacity(), data.len());
    assert_eq!(storage.as_slice(), data);

    let vec = storage.to_vec();
    assert_eq!(vec.capacity(), data.len());
    assert_eq!(vec, data);

    storage.copy_from_slice(&seco);
    storage.flush_sync().expect("To flush");
    assert_eq!(storage.as_slice().len(), seco.len());
    assert_eq!(storage.capacity(), seco.len());
    assert_eq!(storage.as_slice(), seco);

    let vec = storage.to_vec();
    assert_eq!(vec.capacity(), seco.len());
    assert_eq!(vec, seco);
}

#[test]
#[should_panic]
fn test_try_to_copy_into_small_storage() {
    let data = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    let mut storage = Storage::new(1).expect("To crate mmap storage");

    storage.copy_from_slice(&data[..4]);
}
