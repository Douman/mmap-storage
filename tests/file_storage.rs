extern crate mmap_storage;

use std::io::Write;
use std::fs;
use mmap_storage::file::Storage;

#[test]
fn test_it_works() {
    const STORAGE_PATH: &'static str = "test_file.bin";

    let _ = fs::remove_file(STORAGE_PATH);

    {
        let mut storage = Storage::open(STORAGE_PATH).expect("To create storage");
        assert_eq!(storage.capacity(), 1);
        storage.resize(0).expect("Zero file size");
        assert_eq!(storage.capacity(), 1);
        storage.resize(10).expect("Increase file size");
        assert_eq!(storage.capacity(), 10);
        storage.resize(5).expect("Reduce file size");
        assert_eq!(storage.capacity(), 5);
        storage.resize(0).expect("Zero file size");
        assert_eq!(storage.capacity(), 1);

        let data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 1];
        storage.write(&data).expect("To extend with 10 bytes");
        assert_eq!(storage.capacity(), data.len());
        assert_eq!(storage.as_slice(), data);

        storage.write_all(&data).expect("To extend with another 10 bytes");
        assert_eq!(storage.capacity(), data.len() + data.len());
        assert_eq!(storage.as_slice()[..data.len()], data);
        assert_eq!(storage.as_slice()[data.len()..], data);

        storage.resize(data.len()).expect("To resize to 10 bytes");
        assert_eq!(storage.capacity(), data.len());

        let data = [10, 9, 8, 1, 2, 3, 4, 5, 6, 7];
        storage.copy_from_slice(&data);
        assert_eq!(storage.capacity(), data.len());
        assert_eq!(storage.as_slice(), data);

        let data = [2, 3, 4, 5, 6, 7];
        storage.put_data(&data).expect("To put data");
        assert_eq!(storage.capacity(), data.len());
        assert_eq!(storage.as_slice(), data);
        storage.resize(1).expect("Reduce file size to 1");
        assert_eq!(storage.capacity(), 1);
        storage.write(&data[1..]).expect("Write slice of data");
        assert_eq!(storage.capacity(), data.len());
        assert_eq!(storage.as_slice(), data);

        let vec_data = storage.to_vec();
        assert_eq!(vec_data, data);

        drop(storage);

        let storage = Storage::open(STORAGE_PATH).expect("To open storage");
        assert_eq!(storage.capacity(), data.len());
        assert_eq!(storage.as_slice(), data);

    }

    let _ = fs::remove_file(STORAGE_PATH);
}
